#!/bin/bash

MAN="MANUAL:
        USO: $0 [opcion] [parametros]

opciones:       ip - Cambia la ip de la maquina.
		hostname - Cambia el nombre hostname.
                instalar - Podes instalar [php/mysql/pack].
                desinstalar - Podes instalar [apache2/php/mysql/pack].
                sitio - Podes [crear/activar/desactivar] los sitios de apache.
                wordpress - Proceso para setear sitio con wordpress.
                migrar - Podes migrar los sitios hacia otro servidor.
                estadisticas - Podes mirar estadisticas de un sitio.
                backup - Realizar backup.

parametros:     Usando cada opcion veras sus distintos argumentos.
AVISO: La opcion 'wordpress', 'backup' y 'ip' no tienen parametros adicionoales y con solo poner la opcion alcanza."

echo "$MAN"

