#!/bin/bash

# Declaramos variables
START=$(date)
BK=$HOME/backup
BKS=$HOME/backup/logs
ERROR=$HOME/backup/error.logs

read -p "AVISO: Para que el llamado del segundo script funcione tiene que ejecutar este script posicionado en el directorio del mismo script (presione 'enter' para continuar o cancele con 'ctrl c')"

echo "Ejecutando script"
sleep 1s
echo "Cargando..."
sleep 1s
echo "Realizando backup..." && \
# Creamos directorio y archivos
mkdir -p $BK && touch $BKS && echo "Comando mkdir y touch realizado: $(date '+%T')" >> $BKS && \
# Hacemos backup
tar -c -v -z -f $USER.$(date +%b.%e.%Y.%H.%M.%S).tar.gz $HOME 1>/dev/null 2>> $ERROR && echo "Comando tar realizado: $(date '+%T')" >> $BKS && \
echo "Backup realizado con exito" || echo "Backup realizado sin exito"
# Movemos el archivo
mv $USER.$(date +%b.%e.%Y.%H.%M.%S).tar.gz $BK && echo "Comando mv realizado: $(date '+%T')" >> $BKS
sleep 1s
echo "El backup, los logs y los errores se almacenaran en: $BK"
echo "Fecha y hora de inicio del backup: $START" >> $BKS && \
echo "{--------Fecha y hora de finalizacion del backup: $(date)--------}" >>$BKS

sleep 1s
echo "Primera parte del script finalizada"
sleep 1s
echo "~~~Cargando segunda parte~~~"
sleep 1s
# Declaramos variables
CL="/home/global"
UL="/home/$USER/usuario"
AL="/home/global/global.log"
USL="/home/$USER/usuario/user.log"

# Declaramos la variable en etc profile
echo "Hola, tendra que ingresar la contraseña root 3 veces porfavor"
su root -c "echo export LOGINDATE=\$\(date\) >> /etc/profile"
sleep 1s
# Creamos carpeta y archivo para los logs

echo "Ingrese la contraseña del usuario root nuevamente"
su root -c "mkdir -p $CL && chmod o+w $CL && touch $AL && chmod o+w $AL"
mkdir -p "$UL"
touch "$USL"

sleep 1s
# Redireccionamos
echo "Ingrese la contraseña del usuario root por ultima vez"
su root -c "echo 'El usuario $USER ha ingresado el: $LOGINDATE' >> $AL" && \
echo "El usuario $USER ha ingresado el: $LOGINDATE" >> $USL && \
# Final
sleep 1s
echo "Logs finalizados con exito, podes encontrarlos en '$CL' y '$UL'"
sleep 1s
echo "Podras ver el login de los usuarios a partir de cualquier logeo proximo, y ejecucion del script"

sleep 1s
echo "Segunda parte del script finalizada"
sleep 1s
echo "~~~Cargando tercer y ultima parte~~~"
sleep 1s

# Declaramos las variables para el segundo script
VAR1=$(tty)
VAR2=$(date '+%T')
VAR3=$(lastlog -u $USER)

export VAR1=$(tty)
export VAR2=$(date '+%T')
export VAR3=$(lastlog -u $USER)

echo "Se declararon las variables 'VAR1', 'VAR2' y 'VAR3'"
sleep 1s
# LLamamos al segundo script
read -p "Aprete enter para llamar al segundo script"
sleep 1s
echo "Llamando segundo script..."
sleep 1s
bash callscript.sh

