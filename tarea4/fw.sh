#!/bin/bash

if [ $UID -ne 0 ]; then												# Chequeamos que sea root
        echo "Ejecute este programa como 'root'"
        exit 1
fi

if ! which ufw > /dev/null 2>&1 ; then										# Verificamos que este instalado el ufw
        echo "Instalando ufw.."
        apt-get update > /dev/null 2>&1
        apt-get install ufw > /dev/null 2>&1 && echo "Instalado correctamente"
fi
# Variables declaradas
ER='^[0-9]+$' #Expresion regular para que tome numeros
EX='^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$' # Expresion regular para que tome formato ip
ERE='^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\/[0-9]{1,2}$' # Expresion regular para que tome formato red
IP=$4 # Variables para array de ips
IP2=$6
U="Para su uso consulte
	$0 [man]"


case $1 in
man)
	bash man.sh

;;
habilitar)											#Acciones simples como habilitar,
	if [ $2 == fw ]; then									#deshabilitar, status y listado de reglas del wf
		ufw enable
	else
		echo "Opcion invalida: Pruebe [fw]"
	fi
;;
deshabilitar)
	if [ $2 == fw ]; then
		ufw disable
	else
		echo "Opcion invalida: Pruebe [fw]"
	fi
;;
status)
	if [[ $2 == fw ]]; then
		ufw status verbose
	else
		echo "Opcion invalida: Pruebe [fw]"
	fi
;;
listar)
	if [ $2 == reglas ]; then
		ufw status numbered
	else
		echo "Opcion invalida: Pruebe [reglas]"
	fi
;;
reiniciar)
	if [[ $2 == fw ]]; then
		ufw reset
	else
		echo "Opcion invalida: Pruebe [fw]"
	fi
;;
# ----------------------------------default-------------------------
input)													#Cadenas default todas
	if [ $2 == habilitar ]; then									#todas configuradas de igual manera
		ufw default allow incoming								#si no ponen nada de segundo argumento le
	elif [ $2 == deshabilitar ]; then								#aclara que introduzcan, y deben introducir
		ufw default deny incoming								# habilitar o deshabilitar
	else
		echo "Opcion invalida: Pruebe [habilitar/deshabilitar]"
	fi
;;
output)
	if [ $2 == habilitar ]; then
		ufw default allow outgoing
     	elif [ $2 == deshabilitar ]; then
      		ufw default deny outgoing
        else
                echo "Opcion invalida: Pruebe [habilitar/deshabilitar]"
        fi
;;
forward)
	if [ $2 == habilitar ]; then
      		ufw default allow routed
     	elif [ $2 == deshabilitar ]; then
      		ufw default deny routed
        else
                echo "Opcion invalida: Pruebe [habilitar/deshabilitar]"
        fi
;;
# ------------------------------------------------------------------
puerto)
        if [[ $2 == habilitar && $3 =~ $ER ]]; then
                ufw allow $3
        elif [[ $2 == deshabilitar && $3 =~ $ER ]]; then										# Declaracion de reglas primero solo habilitando o no el puerto
                ufw deny $3														# de entrada o salida y despues con ips redes y demas.
	elif [[ $2 == habilitar && $3 == 'in' && $4 =~ $ER ]]; then
		ufw allow in $4
	elif [[ $2 == habilitar && $3 == out && $4 =~ $ER ]]; then
                ufw allow out $4
	elif [[ $2 == deshabilitar && $3 == 'in' && $4 =~ $ER ]]; then
                ufw deny in $4
	elif [[ $2 == deshabilitar && $3 == out && $4 =~ $ER ]]; then
                ufw deny out $4
        else
                echo "Opcion invalida: Pruebe [habilitar/deshabilitar] [numero de puerto] o
			[habilitar/deshabilitar] [in/out] [numero de puerto]"
        fi
;;
ips)
        if [[ $2 == habilitar && $3 == host && $5 == to && $7 =~ $ER ]]; then
                if [[ $IP =~ $EX || $4 == any ]]; then													# Mediante una serie de ifs y arrays validamos que sean ips las que ingresan
                        VIFS=$IFS															# es decir que tengan el formato de 4 octetos y que sea como maximo
                        IFS='.'																# 255.255.255.255
                        IP=($IP)
                        IFS=$VIFS
                        if [[ ${IP[0]} -le 255 && ${IP[1]} -le 255 && ${IP[2]} -le 255 && ${IP[3]} -le 255 ]]; then
                                if [[ $IP2 =~ $EX || $6 == any ]]; then
                                        VIFS=$IFS
                                        IFS='.'
                                        IP2=($IP2)
                                        IFS=$VIFS
                                        if [[ ${IP2[0]} -le 255 && ${IP2[1]} -le 255 && ${IP2[2]} -le 255 && ${IP2[3]} -le 255 ]]; then
                                                ufw allow from $4 to $6 port $7
                                        else
                                                echo "Coloque una IP de destino valida."
                                        fi
                                else
                                        echo "Coloque una IP de destino valida."
                                fi
                        else
                                echo "Coloque una IP de salida valida."
                        fi
                else
                        echo "Coloque una IP de salida valida."
                fi
        elif [[ $2 == deshabilitar && $3 == host && $5 == to && $7 =~ $ER ]]; then
                if [[ $IP =~ $EX || $4 == any ]]; then
                        VIFS=$IFS
                        IFS='.'
                        IP=($IP)
                        IFS=$VIFS
                        if [[ ${IP[0]} -le 255 && ${IP[1]} -le 255 && ${IP[2]} -le 255 && ${IP[3]} -le 255 ]]; then
                                if [[ $IP2 =~ $EX || $6 == any ]]; then
                                        VIFS=$IFS
                                        IFS='.'
                                        IP2=($IP2)
                                        IFS=$VIFS
                                        if [[ ${IP2[0]} -le 255 && ${IP2[1]} -le 255 && ${IP2[2]} -le 255 && ${IP2[3]} -le 255 ]]; then
                                                ufw deny from $4 to $6 port $7
                                        else
                                                echo "Coloque IP de destino valida."
                                        fi
                                else
                                        echo "Coloque IP de destino valida."
                                fi
                        else
                                echo "Coloque IP de salida valida."
                        fi
                else
                        echo "Coloque IP de salida valida."
                fi
	elif [[ $2 == habilitar && $3 == red && $5 == to && $7 =~ $ER ]]; then
                if [[ $IP =~ $ERE || $4 == any ]]; then
                        VIFS=$IFS
                        IFS='.''/'
                        IP=($IP)
                        IFS=$VIFS
                        if [[ ${IP[0]} -le 255 && ${IP[1]} -le 255 && ${IP[2]} -le 255 && ${IP[3]} -le 255 && ${IP[4]} -le 32 ]]; then
                                if [[ $IP2 =~ $ERE || $6 == any ]]; then
                                        VIFS=$IFS
                                        IFS='.''/'
                                        IP2=($IP2)
                                        IFS=$VIFS
                                        if [[ ${IP2[0]} -le 255 && ${IP2[1]} -le 255 && ${IP2[2]} -le 255 && ${IP2[3]} -le 255 && ${IP2[4]} -le 32 ]]; then
                                                ufw allow from $4 to $6 port $7
                                        else
                                                echo "Coloque una red de destino valida."
                                        fi
                                else
                                        echo "Coloque una red de destino valida."
                                fi
                        else
                                echo "Coloque una red de salida valida."
                        fi
                else
                        echo "Coloque una red de salida valida."
                fi
	elif [[ $2 == deshabilitar && $3 == red && $5 == to && $7 =~ $ER ]]; then
                if [[ $IP =~ $ERE || $4 == any ]]; then
                        VIFS=$IFS
                        IFS='.''/'
                        IP=($IP)
                        IFS=$VIFS
                        if [[ ${IP[0]} -le 255 && ${IP[1]} -le 255 && ${IP[2]} -le 255 && ${IP[3]} -le 255 && ${IP[4]} -le 32 ]]; then
                                if [[ $IP2 =~ $ERE || $6 == any ]]; then
                                        VIFS=$IFS
                                        IFS='.''/'
                                        IP2=($IP2)
                                        IFS=$VIFS
                                        if [[ ${IP2[0]} -le 255 && ${IP2[1]} -le 255 && ${IP2[2]} -le 255 && ${IP2[3]} -le 255 && ${IP2[4]} -le 32 ]]; then
                                                ufw deny from $4 to $6 port $7
                                        else
                                                echo "Coloque una red de destino valida."
                                        fi
                                else
                                        echo "Coloque una red de destino valida."
                                fi
                        else
                                echo "Coloque una red de salida valida."
                        fi
                else
                        echo "Coloque una red de salida valida."
                fi
        else
                echo "Opcion invalida: Pruebe [habilitar/deshabilitar] [host/red] [IP de salida] [to] [IP de destino] [numero de puerto]"
                echo "AVISO: Si es un host es x.x.x.x si es una red es x.x.x.x/x"
        fi
;;
insertar)
	if [[ $2 == habilitar && $3 == host && $5 == to && $7 =~ $ER && $8 =~ $ER ]]; then
                if [[ $IP =~ $EX || $4 == any ]]; then                                                                                                  # Mediante una serie de ifs y arrays validamos que sean ips las que ingresan
                        VIFS=$IFS                                                                                                                       # es decir que tengan el formato de 4 octetos y que sea como maximo
                        IFS='.'                                                                                                                         # 255.255.255.255
                        IP=($IP)
                        IFS=$VIFS
                        if [[ ${IP[0]} -le 255 && ${IP[1]} -le 255 && ${IP[2]} -le 255 && ${IP[3]} -le 255 ]]; then
                                if [[ $IP2 =~ $EX || $6 == any ]]; then
                                        VIFS=$IFS
                                        IFS='.'
                                        IP2=($IP2)
                                        IFS=$VIFS
                                        if [[ ${IP2[0]} -le 255 && ${IP2[1]} -le 255 && ${IP2[2]} -le 255 && ${IP2[3]} -le 255 ]]; then
                                                ufw insert $8 allow from $4 to $6 port $7 2> /dev/null || echo "Syntaxis mal escrita o incompleta."
                                        else
                                                echo "Coloque una IP de destino valida."
                                        fi
                                else
                                        echo "Coloque una IP de destino valida."
                                fi
                        else
                                echo "Coloque una IP de salida valida."
                        fi
                else
                        echo "Coloque una IP de salida valida."
                fi
        elif [[ $2 == deshabilitar && $3 == host && $5 == to && $7 =~ $ER && $8 =~ $ER ]]; then
                if [[ $IP =~ $EX || $4 == any ]]; then
                        VIFS=$IFS
                        IFS='.'
                        IP=($IP)
                        IFS=$VIFS
                        if [[ ${IP[0]} -le 255 && ${IP[1]} -le 255 && ${IP[2]} -le 255 && ${IP[3]} -le 255 ]]; then
                                if [[ $IP2 =~ $EX || $6 == any ]]; then
                                        VIFS=$IFS
                                        IFS='.'
                                        IP2=($IP2)
                                        IFS=$VIFS
                                        if [[ ${IP2[0]} -le 255 && ${IP2[1]} -le 255 && ${IP2[2]} -le 255 && ${IP2[3]} -le 255 ]]; then
                                                ufw insert $8 deny from $4 to $6 port $7 2> /dev/null || echo "Syntaxis mal escrita o incompleta."
                                        else
                                                echo "Coloque IP de destino valida."
                                        fi
                                else
                                        echo "Coloque IP de destino valida."
                                fi
                        else
                                echo "Coloque IP de salida valida."
                        fi
                else
                        echo "Coloque IP de salida valida."
                fi
        elif [[ $2 == habilitar && $3 == red && $5 == to && $7 =~ $ER && $8 =~ $ER ]]; then
                if [[ $IP =~ $ERE || $4 == any ]]; then
                        VIFS=$IFS
                        IFS='.''/'
                        IP=($IP)
                        IFS=$VIFS
                        if [[ ${IP[0]} -le 255 && ${IP[1]} -le 255 && ${IP[2]} -le 255 && ${IP[3]} -le 255 && ${IP[4]} -le 32 ]]; then
                                if [[ $IP2 =~ $ERE || $6 == any ]]; then
                                        VIFS=$IFS
                                        IFS='.''/'
                                        IP2=($IP2)
                                        IFS=$VIFS
                                        if [[ ${IP2[0]} -le 255 && ${IP2[1]} -le 255 && ${IP2[2]} -le 255 && ${IP2[3]} -le 255 && ${IP2[4]} -le 32 ]]; then
                                                ufw insert $8 allow from $4 to $6 port $7 2> /dev/null || echo "Syntaxis mal escrita o incompleta."
                                        else
                                                echo "Coloque una red de destino valida."
                                        fi
                                else
                                        echo "Coloque una red de destino valida."
                                fi
                        else
                                echo "Coloque una red de salida valida."
                        fi
                else
                        echo "Coloque una red de salida valida."
                fi
        elif [[ $2 == deshabilitar && $3 == red && $5 == to && $7 =~ $ER && $8 =~ $ER ]]; then
                if [[ $IP =~ $ERE || $4 == any ]]; then
                        VIFS=$IFS
                        IFS='.''/'
                        IP=($IP)
                        IFS=$VIFS
                        if [[ ${IP[0]} -le 255 && ${IP[1]} -le 255 && ${IP[2]} -le 255 && ${IP[3]} -le 255 && ${IP[4]} -le 32 ]]; then
                                if [[ $IP2 =~ $ERE || $6 == any ]]; then
                                        VIFS=$IFS
                                        IFS='.''/'
                                        IP2=($IP2)
                                        IFS=$VIFS
                                        if [[ ${IP2[0]} -le 255 && ${IP2[1]} -le 255 && ${IP2[2]} -le 255 && ${IP2[3]} -le 255 && ${IP2[4]} -le 32 ]]; then
                                                ufw insert $8 deny from $4 to $6 port $7 2> /dev/null || echo "Syntaxis mal escrita o incompleta."
                                        else
                                                echo "Coloque una red de destino valida."
                                        fi
                                else
                                        echo "Coloque una red de destino valida."
                                fi
                        else
                                echo "Coloque una red de salida valida."
                        fi
                else
                        echo "Coloque una red de salida valida."
                fi
	elif [[ $2 == habilitar && $3 =~ $ER ]]; then
                ufw insert $4 allow $3 2> /dev/null || echo "Syntaxis mal escrita o incompleta."
        elif [[ $2 == deshabilitar && $3 =~ $ER ]]; then	                                                                                # Declaracion de reglas primero solo habilitando o no el puerto
                ufw insert $4 deny $3 2> /dev/null || echo "Syntaxis mal escrita o incompleta."                                                  # de entrada o salida y despues con ips redes y demas.
        elif [[ $2 == habilitar && $3 == 'in' && $4 =~ $ER ]]; then
                ufw insert $5 allow in $4 2> /dev/null || echo "Syntaxis mal escrita o incompleta."
        elif [[ $2 == habilitar && $3 == out && $4 =~ $ER ]]; then
                ufw insert $5 allow out $4 2> /dev/null || echo "Syntaxis mal escrita o incompleta."
        elif [[ $2 == deshabilitar && $3 == 'in' && $4 =~ $ER ]]; then
                ufw insert $5 deny in $4 2> /dev/null || echo "Syntaxis mal escrita o incompleta."
        elif [[ $2 == deshabilitar && $3 == out && $4 =~ $ER ]]; then
                ufw insert $5 deny out $4 2> /dev/null || echo "Syntaxis mal escrita o incompleta."
	else
		echo "Opcion invalida: Pruebe [habilitar/deshabilitar] [numero de puerto] [numero de lista] o
                        [habilitar/deshabilitar] [in/out] [numero de puerto] [numero de lista]
			[habilitar/deshabilitar] [host/red] [IP de salida] [to] [IP de destino] [numero de puerto] [numero de lista]"
                echo "AVISO: Si es un host es x.x.x.x si es una red es x.x.x.x/x"
	fi

;;
backup)
	if [[ $2 == reglas ]]; then 										# Backup mandando las copias de los archivos a un directorio
		mkdir -p /var/backups
		echo "BACKUP 'user.rules': $(date)" >> /var/backups/reglas.bk && \
		echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" >> /var/backups/reglas.bk && \
		cat /etc/ufw/user.rules >> /var/backups/reglas.bk && \
		echo "BACKUP 'user6.rules': $(date)" >> /var/backups/reglas.bk && \
                echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" >> /var/backups/reglas.bk && \
                cat /etc/ufw/user6.rules >> /var/backups/reglas.bk && \
		echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" >> /var/backups/reglas.bk && \
		echo "Backup de 'user.rules' y 'user6.rules' realizado."
		chmod o+r /var/backups && chmod o+r /var/backups/reglas.bk
	else
		echo "Opcion invalida: Pruebe [reglas]"
	fi
;;
eliminar)
	if [[ $2 == regla && $3 =~ $ER ]];then								# Nos aseguramos que ponga un numero de la regla valido para eliminar
		ufw delete $3 2> /dev/null || echo "Numero de regla invalido"
	else
		echo "Opcion invalida: pruebe [regla] [numero de regla]"
	fi
;;
*)													# Cuando ingresen algo que no coincida con
	echo "$U"											# el case le muestra esto, que es para que usen el man
	exit
;;
esac
